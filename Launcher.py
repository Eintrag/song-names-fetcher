import DeezerFetcher

def main():
	deezerPlaylistURLFileName = "deezerPlaylistURL.txt"
	with open(deezerPlaylistURLFileName, 'r') as f:
		deezerPlaylistURL = f.readline()
	deezerSongTitles = DeezerFetcher.getDeezerSongTitles(deezerPlaylistURL)
	songListFile = open("songlist.txt", 'w')
	for deezerSongTitle in deezerSongTitles:
		songListFile.write("%s\n" % deezerSongTitle.encode("utf-8"))

if __name__ == "__main__":
    main()

