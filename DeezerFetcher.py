import urllib2
import json

def getDeezerSongTitles(url):
	songs = getSongsFromUrl(url)
	nextUrl = getNextUrl(url)
	while nextUrl:
		songs = songs + getSongsFromUrl(nextUrl)
		nextUrl = getNextUrl(nextUrl)
	return songs

def getNextUrl(url):
	response = urllib2.urlopen(url).read()
	data = json.loads(response)
	try:
		return data["next"]
	except Exception, e:
		return None

def getSongsFromUrl(url):
	response = urllib2.urlopen(url).read()
	data = json.loads(response)
	songsList = []
	for element in data["data"]:
		songsList.append(element["title"] + " " + element["artist"]["name"])
	return songsList

